# Julia_Template_GUI_progress_multithreading

## About

This is a simple template for using Julia with a GUI that shows progress, and has optional multithreading. It can be used with single threaded or multi threaded work. It is written for Linux and can be adapted for MS Windows.

## Requirements

The Gtk.jl package must be added to Julia.

## This template:

- has a very simple project structure with modules in a subfolder.
- shows a basic use of channels. Using channels, its possible to construct workflows containing singlethreaded and multithreaded phases. See the Julia documentation.
- uses directly generated layout, but its also possible to use xml layout files.

## How to run

run from terminal: julia -t auto /path/to/template_threading_progress.jl
"-t auto" is required to use multiple threads. "auto" needs Julia 1.7+. If using earlier versions of Julia, then a number of threads can be set, example "-t 4".
Note: if the script is not started with multiple threads enabled, it will use one thread and the GUI progress will still work.

## Compatibility Statement

This code was written for Linux. 
It is intended as a resource for those wishing to use these features in their projects, but is without guarantee.
All use cases and hardware are not tested. It is not tested in MS Windows.

## Privacy Statement

The code in this project does not collect any data for use outside of the app.

## Freedom Statement

Use the template as you wish for your own project. It uses "The Unlicense". It has no restrictions or obligations. Use without crediting, obligation or payment.

-kodintent
==== end ====
