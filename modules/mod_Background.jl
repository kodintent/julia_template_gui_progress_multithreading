module Background

include("./mod_Functions.jl")
using .Functions 

using Gtk # for updating UI
using Observables
using Base.Threads # to get threadid()

export start_background_work

function start_background_work(dir::AbstractString, channel_collect::Channel, observable_completed::Observable, gtkprogressbar::GtkProgressBarLeaf, gtklabel_progress::GtkLabelLeaf)
    
    update_gtkprogressbar(gtkprogressbar, 0.0)
    update_gtklabel(gtklabel_progress, "gathering files...")

    # EXAMPLE - PREPARE DATA
    array_filepaths = Functions.get_filepaths_in_directory(dir)
    num_filepaths = length(array_filepaths) # NUMBER OF EXPECTED RESULTS

    # SEND OFF LOOPED WORK - RESULTS PUT INTO CHANNEL WHEN COMPLETED
    channel_results = Channel(1) # 1 slot - collected once available
    for filepath in array_filepaths
        Threads.@spawn begin
            # EXAMPLE ACTION FOR EACH FILE (OR LINE, ETC)
            (filename, file_size, datetime) = Functions.get_tuple_from_file(filepath)
            result_from_file = (filename, file_size, datetime, threadid())

            put!(channel_results, result_from_file)
        end
    end

    # COLLECT INTO ARRAY FROM RESULTS CHANNEL, UPDATE UI PROGRESS
    array_results = []
    count_takes = 0
    while isopen(channel_results)
        result_from_file = take!(channel_results)
        count_takes += 1
        push!(array_results, result_from_file)
        update_gtkprogressbar(gtkprogressbar, (count_takes / num_filepaths))
        update_gtklabel(gtklabel_progress, "$count_takes of $num_filepaths") 
        # make sure all results are collected, before closing the channel
        if count_takes == num_filepaths break end
    end
    close(channel_results)

    # PUT COLLECTED RESULTS INTO COLLECTION CHANNEL
    put!(channel_collect, array_results)

    # CHANGE OBSERVABLE TO TRIGGER COLLECTION OF RESULTS IN MAIN THREAD
    observable_completed[] = 1
end

function update_gtklabel(gtklabel::GtkLabelLeaf, labeltext::AbstractString)
    Gtk.GLib.g_idle_add(nothing) do update_ui 
        set_gtk_property!(gtklabel, :label, labeltext)
        Cint(false) # MUST HAVE, MUST BE FALSE
    end
end
function update_gtkprogressbar(gtkprogressbar::GtkProgressBarLeaf, frac::AbstractFloat)
    Gtk.GLib.g_idle_add(nothing) do update_ui
        set_gtk_property!(gtkprogressbar, :fraction, frac)
        Cint(false) # MUST HAVE, MUST BE FALSE
    end
end

end #module
