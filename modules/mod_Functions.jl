module Functions

using Dates

export
get_tuple_from_file,
get_path_child,
get_filepaths_in_directory

function get_tuple_from_file(filepath::AbstractString)
    filename = get_path_child(filepath)
    file_size = 0
    datetime = "mtime-unknown"
    if isfile(filepath)
        file_size = filesize(filepath)
        dt = Dates.unix2datetime(mtime(filepath))
        datetime = Dates.format(dt, "yyyy-mm-dd HH:MM:SS")
    end
    return (filename, file_size, datetime)
end

function get_path_child(path::AbstractString)
    parent, child = splitdir(path)
    return child
end

function get_filepaths_in_directory(dir)
    filepaths = []
    for path in readdir(dir, join=true)
        if isfile(path)
            push!(filepaths, path) 
        end
    end
    return filepaths
end

end #module
