#=
run from terminal: julia -t auto /path/to/template_threading_progress.jl
"-t auto" ("auto" needs Julia 1.7+) is required to use multiple threads.

this example gets some info from files in a selected folder.
it incorporates a simple project structure with module files.
=#

include("modules/mod_Background.jl")
using .Background 
include("modules/mod_Functions.jl")
using .Functions 

using Gtk
using Observables

# strings all in one place
window_title = "Julia GUI with Multithreading and Progress"
text_info = "Template of a simple example with the basic elements of \nprogress, threading, and module files."
text_button_1 = "Select Directory"
text_button_start = "START"
dialog_1_title = string(window_title, " - ", text_button_1)
path_icon = joinpath(homedir(), "sub_homedir/path/to/icon.png")

# GUI generated locally
gtklabel_info = GtkLabel(text_info)
gtkbutton_1 = GtkButton(text_button_1)
gtklabel_button_1 = GtkLabel("")
gtkbutton_start = GtkButton(text_button_start)
gtklabel_progress = GtkLabel("")
gtkprogressbar = GtkProgressBar()
gtklabel_result = GtkLabel("")
set_gtk_property!(gtklabel_result, :wrap, true)
gtkscrolledwindow_result = GtkScrolledWindow(gtklabel_result)
set_gtk_property!(gtkscrolledwindow_result, :min_content_height, 200)

gtkgrid = GtkGrid()
set_gtk_property!(gtkgrid, :column_homogeneous, true)
gtkgrid[1:4,1] = gtklabel_info
gtkgrid[1,2] = gtkbutton_1
gtkgrid[2:4,2] = gtklabel_button_1
gtkgrid[1,3] = gtkbutton_start
gtkgrid[2:4,3] = gtklabel_progress
gtkgrid[2:4,4] = gtkprogressbar
gtkgrid[1:4,5] = gtkscrolledwindow_result
gtkwindow = GtkWindow(window_title)
set_gtk_property!(gtkwindow, :icon, GdkPixbuf(filename=path_icon))
push!(gtkwindow, gtkgrid)
showall(gtkwindow)

observable_completed = Observable(0)
channel_collect = Channel(1)

# BUTTON ACTIONS

function get_dir_bydialog_ornothing(title::AbstractString="")
    dir = open_dialog(title, action=GtkFileChooserAction.SELECT_FOLDER)
    if isdir(dir) return dir end
    return nothing
end

signal_connect(gtkbutton_1, "clicked") do widget
    dir_chosen = get_dir_bydialog_ornothing()
    if isdir(dir_chosen)
        set_gtk_property!(gtklabel_button_1, :label, dir_chosen)
        # clear previous results
        set_gtk_property!(gtkprogressbar, :fraction, 0.0) 
        set_gtk_property!(gtklabel_progress, :label, "")
        set_gtk_property!(gtklabel_result, :label, "")
    end
end

signal_connect(gtkbutton_start, "clicked") do widget
    dir = get_gtk_property(gtklabel_button_1, :label, String)
    if isdir(dir)
        # no returned value, collected from channel
        Threads.@spawn Background.start_background_work(dir, channel_collect, observable_completed, gtkprogressbar, gtklabel_progress)
    else
        set_gtk_property!(gtklabel_button_1, :label, text_button_1)
    end
end

# COLLECT FINAL RESULT OF BACKGROUND WORK 

observable_completed_changed = on(observable_completed) do val
    if observable_completed[] != 0 # to allow reset of UI state
        array_results = []
        while isopen(channel_collect)
            array_results = take!(channel_collect)
            if array_results != [] break end
        end
        #close(channel_collect) # if closed, not usable until app restart

        # EXAMPLE ACTION WITH array_results
        array_results_string = "File info:\n"
        for result in array_results
            (filename, file_size, datetime, thread_id) = result
            array_results_string = string(array_results_string, "File: from threadid $thread_id, $filename is $file_size bytes, last modified $datetime.\n")
        end
        set_gtk_property!(gtklabel_result, :label, array_results_string)  
        observable_completed[] = 0 # reset observable for next start
    end
end

# fake condition keeps window open unless closed.
if !isinteractive()
    c = Condition()
    signal_connect(gtkwindow, :destroy) do widget
        notify(c)
    end
    wait(c)
end


#####################################################################
